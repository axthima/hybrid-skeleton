'use strict';

angular.module('StarterApp', [
  'ngRoute',
  'firebase'
])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/home.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .run(function($rootScope, routeChangeMonitorService, login, $q, $timeout){
    $q.all([login.authReady]).then(function() { // mettre ici les promises de tous les services qui ont besoin d'un petit moment pour s'initialiser
      $rootScope.initialized=true;

      
    });

  })
