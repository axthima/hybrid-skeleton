;(function ($, window, document, undefined) {
  'use strict';

  Foundation.libs.autocloseoffcanvas = {
    name : 'autocloseoffcanvas',

    version : '5.0.3',

    settings : {},

    init : function (scope, method, options) {
      Foundation.inherit(this, 'throttle');
      this.events();
    },

    events : function () {
      $(this.scope).off('autocloseoffcanvas')
        .on('click.fndtn.autocloseoffcanvas', '.close-right-off-canvas', function (e) {
          e.preventDefault();
          $(".off-canvas-wrap").removeClass("move-left");
        })
        .on('click.fndtn.autocloseoffcanvas', '.close-left-off-canvas', function (e) {
          e.preventDefault();
          $(".off-canvas-wrap").removeClass("move-right");
        })

      var self = this;

      $(window)
        .off('.autocloseoffcanvas')
        .on('resize.fndtn.autocloseoffcanvas', self.throttle(function () {
          self.resize.call(self);
        }, 50));

      return this;
    },

    resize : function() {
      if(!matchMedia(Foundation.media_queries.small).matches) {
          $(".off-canvas-wrap").removeClass("move-right");
          $(".off-canvas-wrap").removeClass("move-left");
      }
    },

    reflow : function () {}
  };
}(jQuery, this, this.document));
