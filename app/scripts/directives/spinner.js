'use strict';

angular.module('StarterApp')
  .directive('spinner', function ($timeout) {
    return {
      	restrict: 'A',
      	link: function(scope, element, attrs) {

            var conf = {
                lines: 17, // The number of lines to draw
                length: 0, // The length of each line
                width: 7, // The line thickness
                radius: 12, // The radius of the inner circle
                corners: 0, // Corner roundness (0..1)
                rotate: 0, // The rotation offset
                direction: 1, // 1: clockwise, -1: counterclockwise
                color: '#fbfbfb', // #rgb or #rrggbb or array of colors
                speed: 2.2, // Rounds per second
                trail: 100, // Afterglow percentage
                shadow: false, // Whether to render a shadow
                hwaccel: true, // Whether to use hardware acceleration
                className: 'spinner', // The CSS class to assign to the spinner
                zIndex: 2e9, // The z-index (defaults to 2000000000)
                top: 'auto', // Top position relative to parent in px
                left: 'auto' // Left position relative to parent in px
            };



            var conf1 = angular.copy(conf);
            conf1.color = '#343434';

            var conf2 = angular.copy(conf);
            conf2.radius = 8;
            conf2.width = 5;        

            var conf3 = angular.copy(conf2);
            conf3.color = '#343434';

            var conf4 = angular.copy(conf);
            conf4.radius = 2;
            conf4.width = 4; 

            var conf5 = angular.copy(conf);
            conf5.radius = 20;
            conf5.width = 11; 

            var spinConfs = {spinLargeBlack:conf1, spinSmallWhite:conf2, spinSmallBlack:conf3, spinXtraSmallWhite:conf4, spinXtraLargeWhite:conf5};

            var conf = (attrs.spinnerConf) ? spinConfs[attrs.spinnerConf] : null;
            var spinner = new Spinner(conf)
            var to;
            scope.$watch(attrs.spinner, function(val){
                var setSpinner = function() {
                    if(angular.isDefined(val) && val) {
                        element.toggleClass('spinning', true);

                        if(attrs.spinnerConf) {
                            element.toggleClass('spinning-'+attrs.spinnerConf, true);
                        }
                                
                        if(attrs.spinnerTimeout) {
                            to = setTimeout(function(){
                                spinner.spin(element[0]);
                            }, 0);
                        } else {
                            spinner.spin(element[0]);
                        }
                    } else {
                        if(to) {
                            clearTimeout(to);
                            to = null;
                        }
                        spinner.stop();
                        element.toggleClass('spinning', false);
                        if(attrs.spinnerConf) {
                            element.toggleClass('spinning-'+attrs.spinnerConf, false);
                        }
                    }
                }
                setSpinner();
                    
            });
        }
    };
  });