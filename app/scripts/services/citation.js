angular.module('StarterApp')
	.service('citationService', function(APP_CONFIG, $q, $route, $firebase, $rootScope, login) {
		var _this = this
		
		_this.getAllCitations = function () {
			return $firebase(new Firebase(APP_CONFIG.firebase + 'citations'));
		}

		_this.postCitation = function (citation) {
			if(!login.user) return;

			var data = {
				citation : citation,
				from : login.user.id,
				fromName : login.user.name,
				fromPic : login.user.picture,
				added : Firebase.ServerValue.TIMESTAMP
			};

			return _this.getAllCitations().$add(data);
		}



	});