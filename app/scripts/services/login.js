angular.module('StarterApp')
	.service('login', function(APP_CONFIG, $rootScope, $route, $q, $location, $firebaseSimpleLogin) {
		var _this = this

		var dataRef = new Firebase(APP_CONFIG.firebase);
    	var loginObj = $firebaseSimpleLogin(dataRef);

		var _authReady = $q.defer();
		_this.authReady = _authReady.promise;

		var stopblocking;
		var setupBlocking = function() {
			stopblocking = $rootScope.$on('$locationChangeStart', function(event, newUrl, oldUrl){
		        return event.preventDefault();
		    });
		}
		setupBlocking();

	    _this.authReady.then(function(){
	    	stopblocking();
	    	$route.reload();
	    });

	    loginObj.$getCurrentUser().then(function(user){
	    	_this.user = user;
	    	_authReady.resolve();
	    })

	    $rootScope.$on("$firebaseSimpleLogin:login", function(e, user) {
			_this.user = user;
			console.log(user)
			if(_this.user.provider == 'facebook') _this.user.picture = 'http://graph.facebook.com/' + _this.user.id + '/picture?type=large'
			$location.path('/');
		});

		$rootScope.$on("$firebaseSimpleLogin:logout", function() {
			_this.user = null;
			$location.path('/login');
		});

		_this.checkNotConnected = function () {
			if(_this.user) $location.path('/');
		}

		_this.checkConnected = function() {
			if(!_this.user) $location.path('/login')
		}

		_this.logout = function() {
			loginObj.$logout();
		}

		_this.connect = function(provider) {
			return loginObj.$login(provider, {
				rememberMe : true
			})
		}

	});