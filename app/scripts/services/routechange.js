'use strict';

angular.module('StarterApp')
  .service('routeChangeMonitorService', function ($rootScope, $timeout) {

        $rootScope.$on('$routeChangeStart', function () {
            $rootScope.changingRoute = true;
        });
        $rootScope.$on('$routeChangeSuccess', function () {
        	$timeout(function(){
        		Foundation.init(); // histoire de remettre tous les bindings
        	});
            $rootScope.changingRoute = false;
        });
  });
