;(function(){
    var opts = {
        lines: 17, // The number of lines to draw
        length: 0, // The length of each line
        width: 7, // The line thickness
        radius: 12, // The radius of the inner circle
        corners: 0, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#242424', // #rgb or #rrggbb or array of colors
        speed: 2.2, // Rounds per second
        trail: 100, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: true, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: 'auto', // Top position relative to parent in px
        left: 'auto' // Left position relative to parent in px
    };
    var target = document.getElementById('block_loading');
    var spinner = new Spinner(opts).spin(target);


    var setUp = function() {
        var fontsArray = '10px foundation-icons,10px SourceSansPro-Light,10px SourceSansPro-Regular,10px SourceSansPro-Italic,10px SourceSansPro-Bold,10px GrandHotel'.split(',');
        var fonts = fontsArray.length;
        var fontLoaded = !fonts;
        var setFontLoaded = function(){
            fonts--;
            if(fonts) return
            console.log("Fonts loaded")
            fontLoaded = true;
        }

        console.log("loading fonts")
        for(var i in fontsArray) {
            document.fontloader.loadFont({
                font: fontsArray[i],
                success: setFontLoaded,
                error: setFontLoaded
            });
        }
         
        var bootstrapped

        function start() {
            
          if(!fontLoaded || !window.angular){
            setTimeout(start,100);
            return
          }

          if(bootstrapped) return
          bootstrapped = true

          $(document).foundation();

          angular.element(document).ready(function(){ 
              if(window.stop) window.stop(); // evite le loading infini sous android browser
              setTimeout(function(){
                  angular.bootstrap(angular.element(document.body), ['StarterApp']);
              },0);
          });
        }

        if(document.location.hash == "#phonegap") window.entryScripts.push('cordova.js');

        if(window.entryScripts.length){
          setTimeout(function() { 
            nbl.l(window.entryScripts)
          }, 0);
        } 
        
        start()
    }

    setUp();


})(); 