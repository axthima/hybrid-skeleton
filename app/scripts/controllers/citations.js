'use strict';

angular.module('StarterApp')
  .controller('CitationsCtrl', function ($scope, citationService, login) {

    $scope.getUser = function() {
      return login.user
    }

    $scope.allCitationsLoading = true;

    var citationsAll = citationService.getAllCitations()

    citationsAll.$on('loaded', function() {
        $scope.allCitationsLoading = false;
      });

    citationsAll.$bind($scope, 'citations')


  });
