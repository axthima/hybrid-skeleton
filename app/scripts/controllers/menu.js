'use strict';

angular.module('StarterApp')
  .controller('MenuCtrl', function ($scope, login) {
  	
  	$scope.getUser = function() {
  		return login.user;
  	}
  	$scope.logout = function() {
  		login.logout();
  	}

  });
