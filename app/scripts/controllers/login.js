'use strict';

angular.module('StarterApp')
  .controller('LoginCtrl', function ($scope, login) {
  	login.checkNotConnected();

    $scope.login = function(provider) {
    	if($scope['logging_' + provider]) return;
    	
    	$scope['logging_' + provider] = true;
    	login.connect(provider)['finally'](function() {
    		$scope['logging_' + provider] = false;
    	});
    }

  });
