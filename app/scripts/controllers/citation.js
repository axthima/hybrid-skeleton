
'use strict';

angular.module('StarterApp')
  .controller('CitationCtrl', function ($scope, citationService, $timeout, login) {
  	$timeout(function() {
  		Foundation.libs.equalizer.init()
  	});

  	$scope.editIfPossible = function() {
  		if(login.user.id == $scope.citation.from) return $scope.edit = true;
  	}

  	$scope.closeEdit = function(evt) {
  		if(evt) evt.stopPropagation();
  		$scope.edit = false;
  	}
  });
