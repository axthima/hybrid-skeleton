'use strict';

angular.module('StarterApp')
  .controller('CitationPostCtrl', function ($scope, citationService) {

  	$scope.maxlength = 50;
  	
  	$scope.postCitation = function() {
  		if($scope.postingCitation || !$scope.citation || $scope.citation.length > $scope.maxlength) return
  		$scope.postingCitation = true;

  		citationService.postCitation($scope.citation)['finally'](function(){
  			$scope.postingCitation = false;
  			$scope.citation = '';
  		})
  	}

  });
