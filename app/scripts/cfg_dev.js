'use strict';


angular.module('StarterApp')
    .value('APP_CONFIG', {
        firebase : "https://hybrid-skeleton-dev.firebaseio.com/"
    });